var gulp = require('gulp');
    sass = require('gulp-sass');
    imagemin = require('gulp-imagemin');
    cache = require('gulp-cache');
    useref = require('gulp-useref');
    uglify = require('gulp-uglify');
    gulpIf = require('gulp-if');
    cssnano = require('gulp-cssnano');
    del = require('del');
    runSequence = require('run-sequence');
    babel = require('gulp-babel');
    cache = require('gulp-cache');
    // babelify = require('babelify');
    // browserify = require('gulp-browserify');
    // buffer = require('vinyl-buffer');
    // source = require('vinyl-source-stream');
    // rollup = require('rollup-stream'),


gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss
    .pipe(sass())
    .pipe(gulp.dest('app/css/styles'))
});

// gulp.task('babel', function() {
//     return gulp.src('app/js/**/*.js')
//         .pipe(babel({
//             presets: ['es2015']
//         }))
//         .pipe(gulp.dest('dist'));
// });

gulp.task('watch', ['sass'], function(){
  gulp.watch('app/scss/**/*.scss', ['sass']); 
  gulp.watch('app/*.html'); 
  // gulp.watch('app/js/**/*.js', ['babel']); 
})

// gulp.task('script', function() {
//   return rollup({entry: 'app/js/main.js'})
//     .pipe(source('app/js/main.js'))
//     .pipe(buffer())
//     .pipe(babel())
//     .pipe(gulp.dest('dist'));
// });


gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    // Minifies only if it's a CSS file
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});


gulp.task('images', function(){
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/images'))
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
})

// gulp.task('clean:dist', function() {
//   return del.sync('dist');
// })

// gulp.task('build', function (callback) {
//   runSequence('clean:dist', 
//     ['sass', 'useref', 'images', 'fonts'],
//     callback
//   )
// })

gulp.task('default', function (callback) {
  runSequence(['sass','watch'],
    callback
  )
})



