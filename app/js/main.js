// app.js
var app = angular.module('app', ['ui.router', 'ngAnimate']);

app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/about');
    
    $stateProvider

        .state('about', {
            url: '/about',
            templateUrl: 'app/views/partial-about.html',
            controller: 'inventoryCtrl' 
        })

        .state('home', {
            url: '/home',
            templateUrl: 'app/views/partial-home.html',
            controller: 'inventoryCtrl' 
        })

        .state('products', {
            url: '/products/:id',
            templateUrl: 'app/views/partial-about-item.html',
            controller: 'detailsCtrl'
        });
        
});






