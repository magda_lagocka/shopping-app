app.factory('preloader', [ function($q) {

return {
    start : function(title){
      var defer = $q.defer();
      var preloaderHtml = $('<div class="comp-preloader"><div class="preloaderPopup"></div><div class="preloader-text">'+title+'</div></div>');
      $('body').append(preloaderHtml);
      defer.promise.then(function(){
        preloaderHtml.remove();
      })
      return defer;
    }
  };  
}]);
