'use strict';


app.controller('inventoryCtrl', ['$scope', '$rootScope', 'InventoryFactory', '$filter', '$stateParams', function($scope, $rootScope, InventoryFactory, $filter, $stateParams) {

    var inventory = InventoryFactory('app/default/data.json');

    inventory.price(function(value) {
        //getting the json data
        $scope.price = value.allData;
        $scope.all = value.allData.patientsData.patients;

        sessionStorage.setItem('example', JSON.stringify($scope.all));
        var temp = sessionStorage.getItem('example');
        $scope.myData = $.parseJSON(temp);

        // lazy loading
        $scope.limit = 10;
        
        $scope.loadMore = function() {
            $scope.limit = $scope.myData.length;
        };

        $scope.found = ["terraced", "detached", "apartments"];

        //toggle popup
        $scope.togglePopup = function(value) {
            angular.forEach($scope.myData, function(i) {
                if (i.id === value) {
                    i.showfull = !i.showfull;
                } else {
                    i.showfull = false;
                }
            });
        };

        //price filtering
        $scope.priceArray = [];
        $scope.ranges = [];

        $scope.priceRanging = function(rangeToPush) {
            var i = $.inArray(rangeToPush, $scope.priceArray);
            if (i > -1) {
                $scope.priceArray.splice(i, 1);
                $scope.ranges = rangeToPush.split(',').splice(i, 1);
            } else {
                $scope.priceArray.push(rangeToPush);
            }
            var stringArray = $scope.priceArray.join();

            var splitArray = stringArray.split(',');

            $scope.maxRange = function(splitArray) {
                return Math.max.apply(Math, splitArray);
            };
            $scope.minRange = function(splitArray) {
                return Math.min.apply(Math, splitArray);
            };
            $scope.ranges[1] = $scope.maxRange(splitArray);
            $scope.ranges[0] = $scope.minRange(splitArray);
        };

        $scope.priceFilter = function(value) {
            if ($scope.priceArray.length > 0) {
                if (parseFloat(value.price) >= $scope.ranges[0] && parseFloat(value.price) <= $scope.ranges[1])
                    return value;
            } else {
                return value;
            }
        };

        //  var keys = ["semi-detached", "terraced"];

        //  function filterByID(item) {
        //  if (keys.indexOf(item) != -1) {
        //  return true;
        //  } 
        //  return false; 
        // }

        //showing/hiding favourites cart
        $(function() {
            $('#hidden').hide().click(function(e) {
                e.stopPropagation();
            });
            $("a.cart-buttom").click(function(e) {
                $('#hidden').animate({
                    opacity: "toggle"
                });
                $("#shopping-cart").animate({
                    "height": "toggle"
                }, {
                    duration: 550
                });
                e.stopPropagation();
            });
            $(document).click(function() {
                $('#hidden').fadeOut();
            });
        });


        // favourites cart functionality
        $scope.cart = [];

        function getMatchedValues(item) {
            return $scope.cart.find(function(val) {
                return val.id === item.id;
            });
        }

        $scope.addItem = function(value) {
            var match = getMatchedValues(value);
            if (match) {
                match.quantity += 1;
                return;
            }
            var itemToPush = angular.copy(value);
            itemToPush.quantity = 1;
            if (itemToPush.stock === "unavailable") {
                return;
            } else {
                $scope.cart.push(itemToPush);
                sessionStorage.setItem('session', JSON.stringify($scope.cart));
            }
        };

        // saving properties using sessionStorage in a favourites cart
        if ($.parseJSON(sessionStorage.getItem('session')) !== null) {
            // $scope.cart = $.parseJSON(sessionStorage.getItem('session'));
            $scope.cart = $.parseJSON(sessionStorage["session"]);
        } else {
            $scope.cart = [];
        }

        $scope.deleteItem = function(item) {
            $scope.cart = $.parseJSON(sessionStorage["session"]);
            var match = getMatchedValues(item);
            if (match.quantity > 1) {
                match.quantity -= 1;
                return;
            }
            $scope.cart.splice($scope.cart.indexOf(item), 1);
            sessionStorage["session"] = JSON.stringify($scope.cart);
        };

        $scope.total = function(value) {
            var total = 0;
            angular.forEach($scope.cart, function(item) {
                total += (item.quantity * item.price);
            });
            return total;
        };

    });

}]);