'use strict';

app.directive('activitiesData', ['InventoryFactory', function(InventoryFactory) {
    return {
        restrict: 'A',
        templateUrl: 'app/js/directives/activities/templates/index.html',

        link: function(scope, element, attributes) {

            var house = InventoryFactory('app/default/data.json');
            house.price(function(value) {
                scope.houses = value.allData.patientsData.patients;
        
                var map;

                var myLatLng = new google.maps.LatLng(51.5, -0.13);
                var mapOptions = {
                    center: myLatLng, //Center of our map based on LatLong Coordinates
                    zoom: 11, //How much we want to initialize our zoom in the map
                    //Map type, you can check them in APIs documentation
                };

                //Attaching our features & options to the map
                var map = new google.maps.Map(document.getElementById(attributes.id),
                    mapOptions);
                //Putting a marker on the center
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: "London"
                });
                marker.setMap(map); //Setting the marker up


                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = new google.maps.LatLng(position.coords.latitude,
                            position.coords.longitude);

                        var closestHouse = getNearestHouse(position.coords.latitude, position.coords.longitude);

                        map.setCenter(pos);

                        var myLatlng = new google.maps.LatLng(closestHouse.latitude, closestHouse.longitude);
                        var marker = new google.maps.Marker({

                            position: myLatlng,
                            map: map,
                            title: closestHouse.address
                        });

                    }, function() {
                        handleNoGeolocation(true);
                    });
                } else {

                    handleNoGeolocation(false);
                }

                function handleNoGeolocation(errorFlag) {

                }

                function getNearestHouse(lat, long) {
                    var nearestHouse = null;
                    var nearestDistance = 999999999999999;

                    angular.forEach(scope.houses, function(i) {
                        var oneHouse = i;
                        var distanceToHouse = calcGeoDistance( lat, long, oneHouse.latitude, oneHouse.longitude);
                        if (distanceToHouse < nearestDistance) {
                            nearestDistance = distanceToHouse;
                            nearestHouse = oneHouse;
                        }
                    });
                    return nearestHouse;
                }

                function toRad(Value) {
                    return Value * Math.PI / 180;
                }

                function calcGeoDistance(lat1, lon1, lat2, lon2) {
                    var R = 6371; // km
                    var dLat = toRad(lat2 - lat1);
                    var dLon = toRad(lon2 - lon1);
                    var lat1 = toRad(lat1);
                    var lat2 = toRad(lat2);

                    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                    var d = R * c;
                    return d;
                }

            });

        }
    }
}]);