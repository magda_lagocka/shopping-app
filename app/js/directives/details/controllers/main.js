'use strict';


app.controller('detailsCtrl', ['$scope', '$stateParams', 'InventoryFactory', function($scope, $stateParams, InventoryFactory) {

    var activity = InventoryFactory('app/default/details.json');
    activity.price(function(value) {
        $scope.items = value.detailsData.details;

        $scope.getProduct = function(id) {
            for (var i = 0; i < $scope.items.length; i++) {
                if ($scope.items[i].id == id)
                    return $scope.items[i];
            }
            return null;
        }
        $scope.item = $scope.getProduct($stateParams.id);

        $scope.navigateBack = function() {
            window.history.back();
        };


        $(function() {
            $('img').click(function(e) {

                $("#img01").attr("src", this.src);
                $('#myModal').show();
                e.stopPropagation();
            })
        })

        $(function() {
            $('.close').click(function(e) {
                $('#myModal').hide();
                e.stopPropagation();
            })
        })

    });

}]);